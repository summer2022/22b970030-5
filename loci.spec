Name: loci
Version: 2537db07aa3a3836cd215281e2fe2aa7923706b0
Release: 0
Summary: OpenStack LOCI is a project designed to quickly build Lightweight OCI compatible images of OpenStack services.

License: ASL 2.0
URL: https://opendev.org/openstack/loci
Source0: loci-%{version}.tar.gz

BuildArch: noarch

Requires: docker

%description
OpenStack LOCI is a project designed to quickly build Lightweight OCI compatible images of OpenStack services.

%prep
%setup -q -n loci-%{version}

%build

%install
install -d -m755 %{buildroot}%{_datadir}
cp -vr %{_builddir}/loci-%{version} %{buildroot}%{_datadir}/loci

%files
%{_datadir}/loci

%changelog
* Tue Aug 30 2022 dynos01 <i@dyn.im> - 2537db07aa3a3836cd215281e2fe2aa7923706b0-0
- Initial version